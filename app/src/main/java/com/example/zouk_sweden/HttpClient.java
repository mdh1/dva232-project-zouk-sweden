package com.example.zouk_sweden;

import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;

import java.util.TreeMap;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class HttpClient extends AsyncTask<List<TreeMap<String, Object>>, Void, List<TreeMap<String, Object>>> {

    AsyncResponse Delegate;
    String httpResponse;
    URL url;

    public HttpClient(AsyncResponse delegate)
    {
        this.Delegate = delegate;
    }

    @Override
    public List<TreeMap<String, Object>> doInBackground(List<TreeMap<String, Object>>... params)
    {
        httpResponse = httpApiCall();
        //TreeMap<String,Object> result;
        List<TreeMap<String, Object>> result;

        try
        {
            result = new ObjectMapper().readValue(httpResponse, new TypeReference<List<TreeMap<String, Object>>>(){});
            return result;
        }
        catch (Exception e)
        {
            Log.d("doinbackground", e.toString());
            return null;
        }
    }

    @Override
    protected void onPostExecute(List<TreeMap<String, Object>> result)
    {
        if (result != null)
            Delegate.processFinish(result);
    }

    public String httpApiCall()
    {
        BufferedReader in;
        String response;

        try{
            in = new BufferedReader(new InputStreamReader((this.url.openStream())));
            response = in.readLine();
            in.close();
        }catch (Exception e){
            e.printStackTrace();
            //response = e.toString();
            response = null;
        }
        return response;
    }

    public void setUrlGetAllEvents(Long startDate, Long endDate) {
        try {
            this.url = new URL(SensitiveUrl.getUrlAllEvents(startDate, endDate));
        }
        catch(Exception e) {
            Log.d("setUrlGetAllEvents", e.toString());
        }
    }

    public void setUrlGetAttendees(int id) {
        try {
            this.url = new URL(SensitiveUrl.getUrlAttendees(id));
        }
        catch(Exception e) {
            Log.d("setUrlGetAttendees", e.toString());
        }
    }

    public void setUrlGetEvent(int id) {
        try {
            this.url = new URL(SensitiveUrl.getUrlEvents(id));
        }
        catch(Exception e) {
            Log.d("setUrlGetEvent", e.toString());
        }
    }

    public void setUrlUploadAttendees(int id, String name, String email) {
        try {
            this.url = new URL(SensitiveUrl.getUrlUploadAttendees(id, name, email));
        }
        catch(Exception e) {
            Log.d("setUrlUploadAttendees", e.toString());
        }
    }

    public void setUrlUploadEvent(String title, String description, Long startDate, Long endDate, int color) {
        try {
            this.url = new URL(SensitiveUrl.getUrlUploadEvent(title, description, startDate, endDate, color));
        }
        catch(Exception e) {
            Log.d("setUrlUploadEvent", e.toString());
        }
    }

    public void setUrlUpdateEvent(Integer eventId, String title, String description, Long startDate, Long endDate, Integer color) {
        try {
            this.url = new URL(SensitiveUrl.getUrlUpdateEvent(eventId, title, description, startDate, endDate, color));
        } catch (Exception e) {
            Log.d("setUrlUpdateEvent", e.toString());
        }
    }
    public void setUrlDownloadChat(){
        try{
            this.url = new URL(SensitiveUrl.getUrlDownloadChat());
        }
        catch (Exception e){
            Log.d("setUrlDownloadChat", e.toString());
        }
    }

    public void setUrlUploadChat(String message, String userName, Long date){
        try{
            this.url = new URL(SensitiveUrl.getUrlUploadChat(URLEncoder.encode(message), userName, date));
        }
        catch (Exception e){
            Log.d("setUrlDownloadChat", e.toString());
        }
    }

    public void setUrlCheckPassword(String password) {
        try {
            this.url = new URL(SensitiveUrl.getUrlCheckPassword(password));
        } catch (Exception e) {
            Log.d("setUrlCheckPassword", e.toString());
        }
    }

    public void setUrlGetUsernameEligibility (String username)
    {
        try {
            this.url = new URL(SensitiveUrl.getUrlGetUsernameEligibility(username));
        } catch (Exception e) {
            Log.d("URL username FAILED: ", e.toString());
        }
    }

    public void setUrlSetAdminPassword(String password)
    {
        try {
            this.url = new URL(SensitiveUrl.getUrlSetAdminPassword(password));
        } catch (Exception e) {
            Log.d("URL setPassword FAIL: ", e.toString());
        }
    }

    public void setUrlDeleteEvent(int id) {
        try {
            this.url = new URL(SensitiveUrl.getUrlDeleteEvent(id));
        } catch (Exception e) {
            Log.d("URL username FAILED: ", e.toString());
        }
    }

    public void setUrlCheckChatActivity()
    {
        try {
            this.url = new URL(SensitiveUrl.getUrlCheckChatActivity());
        } catch (Exception e) {
            Log.d("FAILED: ", e.toString());
        }
    }
}