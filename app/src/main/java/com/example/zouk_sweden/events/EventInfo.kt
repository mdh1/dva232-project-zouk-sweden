package com.example.zouk_sweden.events

data class EventInfo (val id: Int, val title: String, val description: String, val endTime: Long)