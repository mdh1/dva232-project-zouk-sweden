package com.example.zouk_sweden.events;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.zouk_sweden.AsyncResponse;
import com.example.zouk_sweden.HttpClient;
import com.example.zouk_sweden.R;
import com.example.zouk_sweden.admin.AdminActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

public class EventActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    TextView titleView,descriptionView, fromTimeView,
            fromDateView, toTimeView, toDateView,
            lastClickedDate, lastClickedTime;
    Spinner colorSpinner;
    Button attendButton;
    RecyclerView attendeeRecycler;

    String dateFormat = "dd MMM YYYY";
    String timeFormat = "HH:mm";

    ArrayList<Attendee> attendees = new ArrayList<>();
    Map<String, Integer> colors = new HashMap<>();
    final Calendar calendar = Calendar.getInstance();

    // old variables, from intent
    int eventId;
    Integer color;
    String title, description;
    Long startTime, endTime;

    // new variables, to be sent to database
    Integer newColor;
    String newTitle, newDescription;
    Long newStartTime, newEndTime;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        Bundle extras = getIntent().getExtras();

        if (extras.get("eventId") != null) {
            eventId = (Integer) extras.get("eventId");
            color = (Integer) extras.get("color");
            title = (String) extras.get("title");
            description = (String) extras.get("description");
            startTime = (Long) extras.get("startTime");
            endTime = (Long) extras.get("endTime");

            newStartTime = startTime;
            newEndTime = endTime;
        }
        else {
            eventId = -1;
            Date date = new Date();
            color = Color.RED;
            title = "";
            description = "";
            if (extras.get("clickedDate") != null) {
                startTime = (Long) extras.get("clickedDate");
                endTime = (Long) extras.get("clickedDate");
            }
            else {
                startTime = date.getTime();
                endTime = date.getTime();
            }

            newStartTime = startTime;
            newEndTime = endTime;
        }

        titleView = this.findViewById(R.id.title);
        descriptionView = this.findViewById(R.id.description);
        colorSpinner = this.findViewById(R.id.colorSpinner);
        fromTimeView = this.findViewById(R.id.fromTime);
        fromDateView = this.findViewById(R.id.fromDate);
        toTimeView = this.findViewById(R.id.toTime);
        toDateView = this.findViewById(R.id.toDate);
        attendButton = this.findViewById(R.id.attendButton);
        attendeeRecycler = this.findViewById(R.id.attendeeRecyclerAdapter);
        
        colors.put("Green", Color.GREEN);
        colors.put("Blue", Color.BLUE);
        colors.put("Magenta", Color.MAGENTA);
        colors.put("Yellow", Color.YELLOW);
        colors.put("Red", Color.RED);

        // Because fuck off, i'm tired
        Map<Integer, String> reverseColors = new HashMap<>();
        for (Map.Entry<String, Integer> e : colors.entrySet()) {
            reverseColors.put(e.getValue(), e.getKey());
        }

        List<String> colorSpinnerValues = new ArrayList<>();
        for (String key : colors.keySet()) {
            colorSpinnerValues.add(key);
        }

        ColorSpinnerAdapter colorSpinnerAdater = new ColorSpinnerAdapter(this, colorSpinnerValues, colors);
        colorSpinner.setAdapter(colorSpinnerAdater);
        colorSpinner.setOnItemSelectedListener(this);
        colorSpinner.setSelection(colorSpinnerValues.indexOf(reverseColors.get(color)));

        String formatedStartTime = new SimpleDateFormat(timeFormat).format(startTime);
        String formatedEndTime = new SimpleDateFormat(timeFormat).format(endTime);

        String startDate = new SimpleDateFormat(dateFormat).format(startTime);
        String endDate = new SimpleDateFormat(dateFormat).format(endTime);

        titleView.setText(title);
        descriptionView.setText(description);
        fromTimeView.setText(formatedStartTime);
        fromDateView.setText(startDate);
        toTimeView.setText(formatedEndTime);
        toDateView.setText(endDate);

        attendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAttendDialog();
            }
        });

        final DatePickerDialog.OnDateSetListener datePickerDialog = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                calendar.set(Calendar.DAY_OF_MONTH, day);
                updateDate(lastClickedDate);
            }
        };

        // Requires API 24, I guess fix it if someone cares enough
        fromDateView.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                lastClickedDate = fromDateView;
                calendar.setTimeInMillis(newStartTime);
                new DatePickerDialog(EventActivity.this, datePickerDialog,
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        // Also requires API 24, of course
        toDateView.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                lastClickedDate = toDateView;
                calendar.setTimeInMillis(newEndTime);
                new DatePickerDialog(EventActivity.this, datePickerDialog,
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        final TimePickerDialog.OnTimeSetListener timePickerDialog = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hour, int minute) {
                calendar.set(Calendar.HOUR_OF_DAY, hour);
                calendar.set(Calendar.MINUTE, minute);
                updateTime(lastClickedTime);
            }
        };

        fromTimeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lastClickedTime = fromTimeView;
                calendar.setTimeInMillis(newStartTime);
                new TimePickerDialog(EventActivity.this, timePickerDialog,
                        calendar.get(Calendar.HOUR_OF_DAY),
                        calendar.get(Calendar.MINUTE),
                        true).show();
            }
        });
        toTimeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lastClickedTime = toTimeView;
                calendar.setTimeInMillis(newEndTime);
                new TimePickerDialog(EventActivity.this, timePickerDialog,
                        calendar.get(Calendar.HOUR_OF_DAY),
                        calendar.get(Calendar.MINUTE),
                        true).show();
            }
        });

        fetchAttendees();
    }

    /*-------------------------------------------Custom methods--------------------------------------------------------*/

    private void initRecyclerView(ArrayList<Attendee> a) {
        attendeeRecycler.setHasFixedSize(true);
        attendeeRecycler.setLayoutManager(new LinearLayoutManager(this));
        attendeeRecycler.setAdapter(new AttendeeRecyclerAdapter(a));
    }

    private void fetchAttendees() {
        attendees = new ArrayList<>();
        AsyncResponse response = new AsyncResponse() {
            @Override
            public void processFinish(List<TreeMap<String, Object>> output) {
                for (int i = output.size() - 1; i >= 0; i--) {
                    attendees.add(new Attendee(output.get(i).get("name").toString(), output.get(i).get("email").toString()));
                }
                initRecyclerView(attendees);
            }
        };

        HttpClient task = new HttpClient(response);
        task.setUrlGetAttendees(eventId);
        task.execute();
    }

    private void updateDate(TextView dateView) {
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.getDefault());
        dateView.setText(sdf.format(calendar.getTimeInMillis()));

        if (dateView == fromDateView) {
            newStartTime = calendar.getTimeInMillis();
        }
        else {
            newEndTime = calendar.getTimeInMillis();
        }

        SimpleDateFormat sdfa = new SimpleDateFormat("YYYY MMM DD HH:mm", Locale.getDefault());
        Toast.makeText(EventActivity.this, sdfa.format(calendar.getTime()), Toast.LENGTH_LONG).show();
    }

    private void updateTime(TextView timeView) {
        SimpleDateFormat sdf = new SimpleDateFormat(timeFormat, Locale.getDefault());
        timeView.setText(sdf.format(calendar.getTimeInMillis()));

        if (timeView == fromTimeView) {
            newStartTime = calendar.getTimeInMillis();
        }
        else {
            newEndTime = calendar.getTimeInMillis();
        }

        SimpleDateFormat sdfa = new SimpleDateFormat("YYYY MMM DD HH:mm", Locale.getDefault());
        Toast.makeText(EventActivity.this, sdfa.format(calendar.getTime()), Toast.LENGTH_LONG).show();
    }

    private void save(int id, String title, String description, Long startDate, Long endDate, int color) {
        AsyncResponse response = new AsyncResponse() {
            @Override
            public void processFinish(List<TreeMap<String, Object>> output) {
            }
            public void progressBarActive() {
            }
            public void progressBarInactive() {
            }
        };

        HttpClient task = new HttpClient(response);

        if (id == -1) { // newly created event
            task.setUrlUploadEvent(title, description, startDate, endDate, color);
        }
        else {
            task.setUrlUpdateEvent(id, title, description, startDate, endDate, color);
        }

        task.execute();
    }

    private void delete(int id) {
        final String message;
        AsyncResponse response = new AsyncResponse() {
            @Override
            public void processFinish(List<TreeMap<String, Object>> output) {
            }
            public void progressBarActive() {
            }
            public void progressBarInactive() {
            }
        };

        HttpClient task = new HttpClient(response);
        task.setUrlDeleteEvent(id);
        task.execute();
    }

    private void attend(int id, String name, String email) {
        AsyncResponse response = new AsyncResponse() {
            @Override
            public void processFinish(List<TreeMap<String, Object>> output) {
            }
            public void progressBarActive() {
            }
            public void progressBarInactive() {
            }
        };

        HttpClient task = new HttpClient(response);
        task.setUrlUploadAttendees(id, name, email);
        task.execute();
        fetchAttendees();
    }

    private void showAttendDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        //builder.setTitle("Attend");

        LayoutInflater inflater = this.getLayoutInflater();
        View layout = inflater.inflate(R.layout.dialog_attend,null);
        builder.setView(layout);

        final EditText name = layout.findViewById(R.id.name);
        final EditText email = layout.findViewById(R.id.email);
        final Button buttonOk = layout.findViewById(R.id.button_ok);
        final Button buttonCancel = layout.findViewById(R.id.button_cancel);

        final AlertDialog dialog = builder.show();

        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!checkIfEmpty(name.getText().toString())) {
                    attend(eventId, name.getText().toString(), email.getText().toString());
                    dialog.dismiss();
                }
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    private boolean checkIfEmpty(String string) {
        boolean isEmpty = true;

        for (int i = 0; i < string.length(); i++) {
            if (string.charAt(i) != ' ') {
                isEmpty = false;
            }
        }
        return isEmpty;
    }

    /*-------------------------------------------Overrides--------------------------------------------------------*/

    @Override
    public void onItemSelected(AdapterView<?> parent, View arg1, int position, long id) {
        String item = parent.getItemAtPosition(position).toString();
        newColor = colors.get(item);
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        if (AdminActivity.getIsAdmin()) {
            inflater.inflate(R.menu.toolbar_event_activity_menu, menu);
        }
        else {
            inflater.inflate(R.menu.toolbar_admin_activity_menu, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save:
                if (AdminActivity.getIsAdmin()){
                    newTitle = titleView.getText().toString();
                    newDescription = descriptionView.getText().toString();

                    if (checkIfEmpty(newTitle)) {
                        Toast.makeText(EventActivity.this, "Title cannot be empty", Toast.LENGTH_LONG).show();
                    }
                    else {
                        if (newColor == color && newTitle == title && newDescription == description && newStartTime == startTime && newEndTime == endTime) {
                            Toast.makeText(EventActivity.this, "Nothing has changed", Toast.LENGTH_LONG).show();
                        }
                        else {
                            save(eventId, newTitle, newDescription, newStartTime, newEndTime, newColor);
                            finish();
                        }
                    }
                    break;
                }
                else {
                    Toast.makeText(EventActivity.this, "Log in as admin first", Toast.LENGTH_LONG).show();
                    break;
                }
            case R.id.discard:
                finish();
                break;
            case R.id.delete:
                if (eventId != -1) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);

                    builder.setTitle("Delete event");
                    builder.setMessage("Are you sure you want to delete this event?");

                    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            delete(eventId);
                            finish();
                        }
                    });

                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            dialog.dismiss();
                        }
                    });

                    final AlertDialog dialog = builder.show();
                }
                break;

        }
        return true;
    }
}
