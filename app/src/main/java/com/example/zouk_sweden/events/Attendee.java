package com.example.zouk_sweden.events;

public class Attendee {
    String name;
    String email;

    public Attendee(String name, String email) {
        this.name = name;
        this.email = email;
    }
}
