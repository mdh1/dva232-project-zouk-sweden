package com.example.zouk_sweden.events;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.github.sundeepk.compactcalendarview.domain.Event;

import com.example.zouk_sweden.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class EventRecyclerAdapter extends RecyclerView.Adapter<EventRecyclerAdapter.MyViewHolder> {
    private List<Event> dataset;
    private boolean displayYear;
    private Fragment fragment;

    public EventRecyclerAdapter(List<Event> newDataset, boolean displayYear, Fragment fragment) {
        this.dataset = newDataset;
        this.displayYear = displayYear;
        this.fragment = fragment;
    }

    @Override
    public EventRecyclerAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_event_item, parent, false);
        return (new MyViewHolder(v));
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        int i = 0;
        for (final Event e : dataset) {
            if (i == position) {
                final EventInfo data = (EventInfo) e.getData();

                String startTime = new SimpleDateFormat("HH:mm").format(e.getTimeInMillis());
                String startDate = new SimpleDateFormat("dd MMM YYYY").format(e.getTimeInMillis());

                String endTime = new SimpleDateFormat("HH:mm").format(data.getEndTime());
                String endDate = new SimpleDateFormat("dd MMM YYYY").format(data.getEndTime());

                holder.colorStrip.setBackgroundColor(e.getColor());
                holder.title.setText(data.getTitle());
                String time = startTime + "\n" + endTime;
                holder.time.setText(time);

                String date = startDate;
                if (!displayYear) {
                    date = "";
                }
                holder.date.setText(date);
                //holder.description.setText(data.getDescription());

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(fragment.getActivity(), EventActivity.class);
                        i.putExtra("eventId", data.getId());
                        i.putExtra("color", e.getColor());
                        i.putExtra("title", data.getTitle());
                        i.putExtra("description", data.getDescription());
                        i.putExtra("startTime", e.getTimeInMillis());
                        i.putExtra("endTime", data.getEndTime());
                        fragment.startActivity(i);
                    }
                });

                return;
            }
            i++;
        }
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, time, description,date;
        public View colorStrip;

        public MyViewHolder(View v) {
            super(v);
            colorStrip = v.findViewById(R.id.event_color_strip);
            title = v.findViewById(R.id.title);
            time = v.findViewById(R.id.time);
            description = v.findViewById(R.id.description);
            date = v.findViewById(R.id.date);
        }
    }
}



