package com.example.zouk_sweden.events;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.zouk_sweden.admin.AdminActivity;

import com.example.zouk_sweden.R;

import java.util.List;

public class AttendeeRecyclerAdapter extends RecyclerView.Adapter<AttendeeRecyclerAdapter.MyViewHolder> {
    private List<Attendee> dataset;

    public AttendeeRecyclerAdapter(List<Attendee> newDataset) {
        this.dataset = newDataset;
    }

    @Override
    public AttendeeRecyclerAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_attendee_item, parent, false);
        return (new MyViewHolder(v));
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        int i = 0;
        for (final Attendee a : dataset) {
            if (i == position) {
                holder.name.setText(a.name);
                if (AdminActivity.getIsAdmin()) { //isNotAdmin
                    holder.email.setText(a.email);
                }
                else {
                    holder.email.setText("");
                }
                return;
            }
            i++;
        }
    }

    private boolean isAdmin() {
        return false;
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, email;

        public MyViewHolder(View v) {
            super(v);
            name = v.findViewById(R.id.name);
            email = v.findViewById(R.id.email);
        }
    }
}



