package com.example.zouk_sweden.events;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.example.zouk_sweden.R;

import java.util.List;
import java.util.Map;

public class ColorSpinnerAdapter  extends BaseAdapter implements SpinnerAdapter {

    List<String> items;
    Map<String, Integer> colors;
    Context context;

    public ColorSpinnerAdapter(Context context, List<String> items, Map<String, Integer> colors) {
        this.items = items;
        this.context = context;
        this.colors = colors;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = View.inflate(context, R.layout.spinner_row_color, null);

        TextView textView = view.findViewById(R.id.name_of_color);
        ImageView imageView = view.findViewById(R.id.color_icon);

        textView.setText(items.get(position));
        imageView.setColorFilter(colors.get(items.get(position)));

        return view;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View view = View.inflate(context, R.layout.spinner_row_color, null);

        TextView textView = view.findViewById(R.id.name_of_color);
        ImageView imageView = view.findViewById(R.id.color_icon);

        textView.setText(items.get(position));
        imageView.setColorFilter(colors.get(items.get(position)));

        return view;
    }
}
