package com.example.zouk_sweden;
import java.util.List;
import java.util.TreeMap;

public interface AsyncResponse{
    void processFinish(List<TreeMap<String, Object>> output);
}
