package com.example.zouk_sweden.ui.calendar;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.zouk_sweden.AsyncResponse;
import com.example.zouk_sweden.HttpClient;
import com.example.zouk_sweden.MainActivity;
import com.example.zouk_sweden.admin.AdminActivity;
import com.example.zouk_sweden.events.EventActivity;
import com.example.zouk_sweden.events.EventInfo;
import com.example.zouk_sweden.events.EventRecyclerAdapter;
import com.example.zouk_sweden.R;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
//import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TreeMap;

public class CalendarFragment extends Fragment {

    private RecyclerView eventRecycler;
    private ArrayList<Event> eventList = new ArrayList<>();
    private CompactCalendarView compactCalendar;

    Date lastClickedDate = new Date();

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_calendar, container, false);
        eventRecycler = root.findViewById(R.id.eventRecycler);
        compactCalendar = root.findViewById(R.id.compactCalendar_view);
        FloatingActionButton fabAddEvent = root.findViewById(R.id.fabAddEvent);

        ((MainActivity) getActivity()).setKill(false);

        SharedPreferences sharedPreferences;
        SharedPreferences.Editor editor;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        if (AdminActivity.getIsAdmin()) {
            fabAddEvent.show();
        }
        else {
            fabAddEvent.hide();
        }

        Date date = new Date();
        compactCalendar.shouldDrawIndicatorsBelowSelectedDays(true);

        compactCalendar.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                initRecyclerView(compactCalendar.getEvents(dateClicked));
                lastClickedDate = dateClicked;
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                updateDateTitle(firstDayOfNewMonth);
                lastClickedDate = firstDayOfNewMonth;
                updateCalendarData(firstDayOfNewMonth);
                initRecyclerView(compactCalendar.getEvents(firstDayOfNewMonth));
            }
        });

        fabAddEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(view.getContext(), EventActivity.class);
                i.putExtra("clickedDate", lastClickedDate.getTime());
                startActivity(i);
                //startActivity(new Intent(view.getContext(), EventActivity.class));
            }
        });

        //initRecyclerView(compactCalendar.getEvents(date));

        editor = sharedPreferences.edit();
        editor.putBoolean("fragment", true);
        editor.apply();

        return root;
    }

    private void initRecyclerView(List<Event> e) {
        eventRecycler.setHasFixedSize(true);
        eventRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        eventRecycler.setAdapter(new EventRecyclerAdapter(e, false, this));
    }

    private void initCalendar() {
        populateCalendar();
    }

    private void updateDateTitle(Date date) {
        SimpleDateFormat currDate = new SimpleDateFormat("MMM YYYY", Locale.getDefault());
        String activeDate = currDate.format(date);
        ((MainActivity) getActivity()).setActionBarTitle(activeDate);
    }

    private void updateCalendarData(final Date date){
        compactCalendar.removeAllEvents();
        eventList.clear();

        AsyncResponse response = new AsyncResponse() {
            @Override
            public void processFinish(List<TreeMap<String, Object>> output)
            {
                for (int i = 0; i < output.size(); i++)
                {
                    eventList.add(new Event(Integer.parseInt(output.get(i).get("color").toString()),
                            Long.parseLong(output.get(i).get("startTime").toString()),
                            new EventInfo(
                                    Integer.parseInt(output.get(i).get("id").toString()),
                                    output.get(i).get("title").toString(),
                                    output.get(i).get("description").toString(),
                                    Long.parseLong(output.get(i).get("endTime").toString()))));
                }
                initCalendar();
                initRecyclerView(compactCalendar.getEvents(date));
            }
        };

        HttpClient task = new HttpClient(response);
        task.setUrlGetAllEvents(date.getTime() - 5356800000L, date.getTime() + 5356800000L); //5356800000L = 2 months
        task.execute();
    }

    private void populateCalendar() {
        compactCalendar.addEvents(eventList);
    }

    @Override
    public void onResume() {  // After a pause OR at startup
        super.onResume();
        //Date date = new Date();
        //Refresh your stuff here
        eventList.clear();
        updateDateTitle(lastClickedDate);
        updateCalendarData(lastClickedDate);
    }
}


