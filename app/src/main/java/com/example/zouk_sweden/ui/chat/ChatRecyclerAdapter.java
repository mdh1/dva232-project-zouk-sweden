package com.example.zouk_sweden.ui.chat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.zouk_sweden.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class ChatRecyclerAdapter extends RecyclerView.Adapter<ChatRecyclerAdapter.MyViewHolder> {
    private List<Message> dataset;

    public ChatRecyclerAdapter(List<Message> newDataset) {
        dataset = newDataset;
    }

    @Override
    public ChatRecyclerAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new views
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_chat_item, parent, false);
        return (new MyViewHolder(v));
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        int i = 0;
        for (Message m : dataset) {
            if (position == i) {
                if (m.isUser) {
                    holder.my_message.setText(m.message);
                    holder.my_userName.setText(m.userName);

                    holder.message.setVisibility(View.GONE);
                    holder.userName.setVisibility(View.GONE);
                    holder.time.setVisibility(View.GONE);

                    holder.my_message.setVisibility(View.VISIBLE);
                    holder.my_userName.setVisibility(View.VISIBLE);
                    holder.my_time.setVisibility(View.VISIBLE);
                }
                else{

                    holder.message.setText(m.message);
                    holder.userName.setText(m.userName);

                    holder.my_message.setVisibility(View.GONE);
                    holder.my_userName.setVisibility(View.GONE);
                    holder.my_time.setVisibility(View.GONE);

                    holder.message.setVisibility(View.VISIBLE);
                    holder.userName.setVisibility(View.VISIBLE);
                    holder.time.setVisibility(View.VISIBLE);
                }
                //format m.time into a HH:mm string (ex. 14:45)
                final Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(m.time);
                String time = new SimpleDateFormat("HH:mm").format(cal.getTime());

                holder.time.setText(time);
                holder.my_time.setText(time);
            }
            i++;
        }
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView message, userName, time, my_message, my_userName, my_time;

        public MyViewHolder(View v) {
            super(v);
            message = v.findViewById(R.id.message);
            userName = v.findViewById(R.id.userName);
            time = v.findViewById(R.id.time);

            my_message = v.findViewById(R.id.my_message);
            my_userName = v.findViewById(R.id.my_userName);
            my_time = v.findViewById(R.id.my_time);
        }
    }
}