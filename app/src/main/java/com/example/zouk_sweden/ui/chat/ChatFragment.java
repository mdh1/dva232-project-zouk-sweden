package com.example.zouk_sweden.ui.chat;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.fragment.app.Fragment;
import androidx.annotation.NonNull;

import androidx.recyclerview.widget.RecyclerView;
import com.example.zouk_sweden.R;
import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.WindowManager;
import android.view.ViewGroup;
import android.view.View;

import android.widget.EditText;
import android.widget.Button;

import java.util.TreeMap;
import java.util.Date;
import java.util.List;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.ProgressBar;

import static android.content.ContentValues.TAG;
import com.example.zouk_sweden.*;

public class ChatFragment extends Fragment {
    private RecyclerView eventRecycler;
    private ArrayList<Message> messages = new ArrayList<>();
    private EditText chatMessage;
    private View root;
    private static String userName;
    private AlertDialog.Builder alert;
    private Button sendMessageButton;
    private AlertDialog alertWindow;
    private static Boolean isUserMe;
    private ProgressBar progressBar;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_chat, container, false);
        eventRecycler = root.findViewById(R.id.chatWindow);
        alert = new AlertDialog.Builder(getActivity());
        progressBar = root.findViewById(R.id.progressBar);

        alert.setTitle("Choose your username");
        alert.setMessage("");
        final EditText tempInput = new EditText(getActivity());
        alert.setView(tempInput);


        ((MainActivity) getActivity()).setKill(false);

        SharedPreferences sharedPreferences;
        SharedPreferences.Editor editor;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());


        editor = sharedPreferences.edit();
        editor.putString("fragment", "chat");
        editor.apply();

        if (savedInstanceState != null) {
            userName = savedInstanceState.getString(userName);
        }

        if (sharedPreferences.getString("username", null) != null) {
            userName = sharedPreferences.getString("username", null);
        }
        sendMessageButton = root.findViewById(R.id.chatSendMessage);
        sendMessageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userName != null)
                {
                    onClickChatSendMessage(v);
                }
            }
        });
        final AsyncResponse temp;

        MainActivity.setChatIconToDefault();

        temp = new AsyncResponse() {
            @Override
            public void processFinish(List<TreeMap<String, Object>> output) {

                if ((output.size() == messages.size()) && (messages.size() != 0)) {
                    return;
                }

                for (int i = 0; i < output.size(); i++) {
                    try {
                        messages.get(i);
                    }
                    catch(IndexOutOfBoundsException e){ // Om Messages inte har värde på get(i) kommer den att ge exception -> vi vet att den är tom lägg till nya grejer från db.
                        if (output.get(i).get("userName").toString().equals(userName))
                            isUserMe = true;
                        else
                            isUserMe = false;

                        messages.add(new Message(
                                output.get(i).get("message").toString(),
                                output.get(i).get("userName").toString(),
                                Long.parseLong(output.get(i).get("time").toString()),
                                isUserMe
                        ));
                    }
                }
                progressBar.setVisibility(View.GONE);
                initRecyclerView(messages);
            }
        };

        final Handler handler = new Handler();
        Runnable refresh = new Runnable() {
            @Override
            public void run() {
                try {
                    HttpClient task = new HttpClient(temp);
                    task.setUrlDownloadChat();
                    task.execute();
                }
                catch (NullPointerException e) {
                    Log.e(TAG, "" + e);
                }
                handler.postDelayed(this, 1000);
            }
        };
        handler.postDelayed(refresh, 1000);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                checkUserNameEligibility(tempInput.getText().toString());
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                sendMessageButton.setEnabled(false);
            }
        });

        if (userName == null || userName.isEmpty()) {
             alertWindow = alert.show();
        }

        return root;
    }
    private void checkUserNameEligibility(final String usrName)
    {
        AsyncResponse temp = new AsyncResponse() {
            @Override
            public void processFinish(List<TreeMap<String, Object>> output) { // databas returnerar 0 om ingen användare fanns = false annars true

                if(Integer.parseInt(output.get(0).get("count").toString()) > 0 || isNotAlphabetic(usrName)) {
                    userName = null;
                    alertWindow.dismiss();
                    alertWindow.setMessage("username already in use or outside a-z characters!");
                    alertWindow.show();
                }
                else {
                    userName = usrName;
                    editor = sharedPreferences.edit();
                    editor.putString("username", usrName);
                    editor.apply();
                }
            }
        };
        HttpClient client = new HttpClient(temp);

        client.setUrlGetUsernameEligibility(usrName);
        client.execute();
    }

    private boolean isNotAlphabetic(String user)
    {
        char[] usrname = user.toCharArray();
        for (char c : usrname)
        {
            if (!Character.isLetter(c)){
                return true;
            }
        }
        return false;
    }

    private void onClickChatSendMessage(View v)
    {
        chatMessage = root.findViewById(R.id.chatMessage);

        String messageToSend = chatMessage.getText().toString();
        if (messageToSend.isEmpty())
        {
            return;
        }
        HttpClient task = new HttpClient(chatSendMessagePrepAsyncResponse());
        Date date = new Date();
        task.setUrlUploadChat(messageToSend, userName, date.getTime());
        task.execute();

        chatMessage.setText("");
    }

    private AsyncResponse chatSendMessagePrepAsyncResponse()
    {
        AsyncResponse delegate;

        delegate = new AsyncResponse() {
            @Override
            public void processFinish(List<TreeMap<String, Object>> output) {

            }
        };
        return delegate;
    }

    private void initRecyclerView(ArrayList<Message> m)
    {
        eventRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        try {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        }
        catch (NullPointerException e){
            Log.e(TAG, "" + e);
            return;
        }
        eventRecycler.setAdapter(new ChatRecyclerAdapter(m));
        eventRecycler.scrollToPosition(messages.size()-1);
    }

    @Override
    public void onPause(){
        MainActivity.setChatIsActive(false);
        super.onPause();
    }

    @Override
    public void onResume(){
        MainActivity.setChatIsActive(true);
        MainActivity.setChatIconToDefault();
        super.onResume();
    }
}