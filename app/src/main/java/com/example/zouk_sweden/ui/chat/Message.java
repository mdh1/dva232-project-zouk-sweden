package com.example.zouk_sweden.ui.chat;

public class Message {
    String message;
    String userName;
    Long time;
    Boolean isUser;

    public Message(String message, String userName, Long timeInMillis, Boolean isUser) {
        this.message = message;
        this.userName = userName;
        this.time = timeInMillis;
        this.isUser = isUser;
    }
}
