package com.example.zouk_sweden.ui.home.current;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.zouk_sweden.R;
import com.example.zouk_sweden.events.EventInfo;
import com.example.zouk_sweden.events.EventRecyclerAdapter;
import com.example.zouk_sweden.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;

import com.github.sundeepk.compactcalendarview.domain.Event;

public class CurrentFragment extends Fragment {
    RecyclerView eventRecycler;
    List<Event> eventList = new ArrayList<>();

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home_fragment_current, container, false);

        eventRecycler = root.findViewById(R.id.currentEventsRecycler);

        ((MainActivity) getActivity()).setKill(true);

        return root;
    }

    private void initRecyclerView(List<Event> e) {
        eventRecycler.setHasFixedSize(true);
        eventRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        eventRecycler.setAdapter(new EventRecyclerAdapter(e, true, this));
    }

    private void updateEventList() {
        AsyncResponse temp = new AsyncResponse() {
            @Override
            public void processFinish(List<TreeMap<String, Object>> output)
            {
                for (int i = 0; i < output.size(); i++)
                {
                    eventList.add(new Event(Integer.parseInt(output.get(i).get("color").toString()),
                            Long.parseLong(output.get(i).get("startTime").toString()),
                            new EventInfo(
                                    Integer.parseInt(output.get(i).get("id").toString()),
                                    output.get(i).get("title").toString(),
                                    output.get(i).get("description").toString(),
                                    Long.parseLong(output.get(i).get("endTime").toString()))));
                }
                initRecyclerView(eventList);
            }
        };

        HttpClient task = new HttpClient(temp);
        Date date = new Date();
        task.setUrlGetAllEvents(date.getTime(), date.getTime() + 2592000000L);
        task.execute();
    }

    @Override
    public void onResume() {  // After a pause OR at startup
        super.onResume();
        //Date date = new Date();
        //Refresh your stuff here
        eventList.clear();
        updateEventList();
    }
}
