package com.example.zouk_sweden.ui.home.about;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.example.zouk_sweden.R;

public class AboutFragment extends Fragment {

    ConstraintLayout expandableViewZ;
    Button arrowBtnZ;
    ConstraintLayout topViewZ;


    ConstraintLayout expandableViewR;
    Button arrowBtnR;
    ConstraintLayout topViewR;

    ImageButton facebookZ;
    ImageButton facebookR;
    ImageButton instagramZ;
    ImageButton instagramR;
    ImageButton youtubeR;
    ImageButton youtubeZ;



    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
       final View root = inflater.inflate(R.layout.fragment_home_fragment_about, container, false);

        expandableViewZ = root.findViewById(R.id.expandableView);
        expandableViewR=root.findViewById(R.id.expandableView_ramalhos);
        arrowBtnR= root.findViewById(R.id.arrowBtn2);
        arrowBtnZ = root.findViewById(R.id.arrowBtn);
        topViewR=root.findViewById(R.id.top_layout_R);
        topViewZ=root.findViewById(R.id.top_layout_Z);


        facebookZ = root.findViewById(R.id.facebook_buttonZ);
        facebookR = root.findViewById(R.id.facebook);
        instagramR = root.findViewById(R.id.instagram);
        instagramZ= root.findViewById(R.id.instagram_buttonZ);
        youtubeR= root.findViewById(R.id.youtube);
        youtubeZ= root.findViewById(R.id.youtube_zouksweden);


        arrowBtnZ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expandableViewOnClick(expandableViewZ, arrowBtnZ);
            }
        });

        arrowBtnR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expandableViewOnClick(expandableViewR, arrowBtnR);

            }
        });


        topViewR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expandableViewOnClick(expandableViewR, arrowBtnR);

            }
        });

        topViewZ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expandableViewOnClick(expandableViewZ, arrowBtnZ);
            }
        });




        facebookZ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent facebookIntent = getOpenFacebookIntent(root.getContext(), "313669509069439", "Zouk-Sweden-313669509069439/");
                startActivity(facebookIntent);

            }
        });

        facebookR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent facebookIntent = getOpenFacebookIntent(root.getContext(), "290903844863091", "Zouk-Sweden-313669509069439/");
                startActivity(facebookIntent);

            }
        });

       instagramZ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent instagramIntent = getOpenInstagramIntent(root.getContext(), "zouksweden_etuna");
                startActivity(instagramIntent);
            }
        });

        instagramR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent instagramIntent = getOpenInstagramIntent(root.getContext(), "theramalhoszouk");
                startActivity(instagramIntent);
            }
        });

        youtubeZ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent instagramIntent = getOpenYoutubeIntent(root.getContext(), "UC119mkjAbG5-CjHRzTtf5dg");
                startActivity(instagramIntent);
            }
        });

        youtubeR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent instagramIntent = getOpenYoutubeIntent(root.getContext(), "UCtQhHG2rxMsHGRREl1NQhZw");
                startActivity(instagramIntent);
            }
        });


        return root;
    }


    public void expandableViewOnClick(ConstraintLayout exView, Button arrow){
        if (exView.getVisibility()==View.GONE){
            TransitionManager.beginDelayedTransition(exView, new AutoTransition());
            exView.setVisibility(View.VISIBLE);
            arrow.setBackgroundResource(R.drawable.ic_keyboard_arrow_up_black_24dp);
        } else {
            exView.setVisibility(View.GONE);
            arrow.setBackgroundResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
        }
    }

    public static Intent getOpenFacebookIntent(Context context, String id, String username) {

        try {
            context.getPackageManager().getPackageInfo("com.facebook.katana", 0);
            return new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/" + id));
        } catch (Exception e) {
            return new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/"+ username + "/" ));
        }
    }

    public static Intent getOpenInstagramIntent( Context context, String username) {

        String scheme = "http://instagram.com/_u/" + username;
        String path = "https://instagram.com/" + username;
        String nomPackageInfo ="com.instagram.android";
        try {
            context.getPackageManager().getPackageInfo(nomPackageInfo, 0);
            return new Intent(Intent.ACTION_VIEW, Uri.parse(scheme));
        } catch (Exception e) {
            return new Intent(Intent.ACTION_VIEW, Uri.parse(path));
        }

    }

    public static Intent getOpenYoutubeIntent( Context context, String username) {

        String scheme = "https://www.youtube.com/channel/" + username;
        String path = "https://www.youtube.com/channel/" + username;
        String nomPackageInfo ="com.google.android.youtube";
        try {
            context.getPackageManager().getPackageInfo(nomPackageInfo, 0);
            return new Intent(Intent.ACTION_VIEW, Uri.parse(scheme));
        } catch (Exception e) {
            return new Intent(Intent.ACTION_VIEW, Uri.parse(path));
        }}

}
