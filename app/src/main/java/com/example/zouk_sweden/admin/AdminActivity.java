package com.example.zouk_sweden.admin;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.example.zouk_sweden.AsyncResponse;
import com.example.zouk_sweden.HttpClient;
import com.example.zouk_sweden.R;
import com.example.zouk_sweden.events.EventInfo;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.List;
import java.util.TreeMap;

public class AdminActivity extends AppCompatActivity {
    private static boolean isAdmin=false;
    EditText changePasswordBox;
    EditText changePasswordBoxTypeAgain;
    String passwordBox;
    String typeAgainPasswordBox;
    private AlertDialog.Builder alert;
    private AlertDialog alertWindow;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    AsyncResponse delegate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        super.onCreate(savedInstanceState);

        getSupportActionBar().setElevation(0);
        setContentView(R.layout.activity_admin);
        final CardView changePasswordButton = (CardView) findViewById(R.id.changePasswordButton);
        final CardView logoutButton = (CardView) findViewById(R.id.LogoutButton);
        changePasswordBox = findViewById(R.id.changePasswordBox);
        changePasswordBoxTypeAgain = findViewById(R.id.changePasswordtypeAgain);
        alert = new AlertDialog.Builder(this);

        delegate = new AsyncResponse() {
            @Override
            public void processFinish(List<TreeMap<String, Object>> output) {
                if(output.get(0).get("result").toString().equals("true") ) { // Something went wrong with the internet/DB
                    editor = sharedPreferences.edit();
                    editor.putBoolean("isAdmin", true);
                    editor.apply();

                    alert.setTitle("Set password operation successful");
                    alert.setMessage("Your password has been updated!");
                    alert.show();

                }
                else {
                    alert.setTitle("Set password operation failure");
                    alert.setMessage("Unknown error");
                    alert.show();
                }
            }
        };

        changePasswordButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v)
            {
                passwordBox = changePasswordBox.getText().toString();
                typeAgainPasswordBox = changePasswordBoxTypeAgain.getText().toString();

                if ((!passwordBox.equals(typeAgainPasswordBox)) || passwordBox.isEmpty() || typeAgainPasswordBox.isEmpty()){
                    alert.setTitle("Set password operation failure");
                    alert.setMessage("Passwords either do not match or empty");
                    alert.show();
                    changePasswordBox.setText(null);
                    changePasswordBoxTypeAgain.setText(null);
                    return;
                }
                newAdminPasswordAsyncPrep(passwordBox);
            }
        });

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });

        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setIsAdmin(false);
                editor = sharedPreferences.edit();
                editor.remove("isAdmin");
                editor.apply();
                finish();
            }
        });
    }

    private void newAdminPasswordAsyncPrep(String password) {
        HttpClient client = new HttpClient(delegate);
        client.setUrlSetAdminPassword(password);
        client.execute();
    }

    public static void setIsAdmin(boolean status) {
        isAdmin = status;
    }

    public static boolean getIsAdmin() {
        return isAdmin;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.toolbar_admin_activity_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.discard:
                finish();
                break;
        }
        return true;
    }
}
