package com.example.zouk_sweden.admin;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.example.zouk_sweden.AsyncResponse;
import com.example.zouk_sweden.HttpClient;
import com.example.zouk_sweden.R;

import java.util.List;
import java.util.TreeMap;

public class LoginActivity extends AppCompatActivity {
    EditText passwordBox;
    CardView loginButton;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        getSupportActionBar().setElevation(0);

        passwordBox = findViewById(R.id.passwordBox);
        loginButton = findViewById(R.id.loginButton);

        if (sharedPreferences.getBoolean("isAdmin", false))
        {
            AdminActivity.setIsAdmin(true);
            openAdminActivity();
        }

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AsyncResponse response = new AsyncResponse() {
                    @Override
                    public void processFinish(List<TreeMap<String, Object>> output)
                    {
                        int count = Integer.parseInt(output.get(0).get("count").toString());
                        if (count>0){
                            AdminActivity.setIsAdmin(true);
                        }

                        if (AdminActivity.getIsAdmin()) {
                            editor = sharedPreferences.edit();
                            editor.putBoolean("isAdmin", true);
                            editor.apply();
                            openAdminActivity();
                        }

                        else {
                            prompt("Wrong password");
                        }
                    }
                };

                HttpClient task = new HttpClient(response);
                task.setUrlCheckPassword(passwordBox.getText().toString());
                task.execute();
            }
        });
    }

    private void openAdminActivity() {
        Intent i = new Intent(this, AdminActivity.class);
        startActivity(i);
    }

    private void prompt(String message) {
        Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.toolbar_admin_activity_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.discard:
                finish();
                break;
        }
        return true;
    }
}
