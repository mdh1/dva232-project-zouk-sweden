package com.example.zouk_sweden;

import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;

import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.zouk_sweden.admin.AdminActivity;
import com.example.zouk_sweden.admin.LoginActivity;
import com.example.zouk_sweden.ui.chat.ChatFragment;
import com.example.zouk_sweden.ui.home.current.CurrentFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.appcompat.app.AppCompatActivity;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;

import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import java.util.List;
import java.util.TreeMap;

import static android.content.ContentValues.TAG;
import static com.example.zouk_sweden.AppNotification.CHANNEL_1_ID;

public class MainActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    Menu menu;
    AsyncResponse temp;
    static MenuItem item;
    Long currentChatTimeStamp;
    static Boolean isOnChatPage;

    static boolean kill = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        isOnChatPage = false;

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        if (sharedPreferences.getBoolean("isAdmin", false)) {
            AdminActivity.setIsAdmin(sharedPreferences.getBoolean("isAdmin", false));
        }

        if (!sharedPreferences.getBoolean("accepted_terms", false)) {
            showNoticeDialog();
        }

        getSupportActionBar().setElevation(0);
        BottomNavigationView navView = findViewById(R.id.nav_view);

        item = navView.getMenu().getItem(2);
        //item.setIcon(android.R.drawable.ic_lock_idle_lock);

        temp = new AsyncResponse() {
            @Override
            public void processFinish(List<TreeMap<String, Object>> output) {

                if (isOnChatPage){
                    currentChatTimeStamp = Long.parseLong(output.get(0).get("time").toString());
                    return;
                }

                if (currentChatTimeStamp == null) {
                    currentChatTimeStamp = Long.parseLong(output.get(0).get("time").toString());
                    return;
                }
                if (currentChatTimeStamp < Long.parseLong(output.get(0).get("time").toString())) {
                    item.setIcon(R.drawable.ic_action_message_alert);
                    currentChatTimeStamp = Long.parseLong(output.get(0).get("time").toString());
                    sendNotification(output.get(0).get("userName").toString(), output.get(0).get("message").toString());
                }
            }
        };

        final Handler handler = new Handler();
        Runnable refresh = new Runnable() {
            @Override
            public void run() {
                try {
                    HttpClient task = new HttpClient(temp);
                    task.setUrlCheckChatActivity();
                    task.execute();
                }
                catch (NullPointerException e) {
                    Log.e(TAG, "" + e);
                }
                handler.postDelayed(this, 1000);
            }
        };
        handler.postDelayed(refresh, 1000);

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_calendar, R.id.navigation_chat)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);
    }

    public static void setChatIconToDefault(){
        item.setIcon(R.drawable.ic_action_message);
    }

    public static void setChatIsActive(Boolean status)
    {
        isOnChatPage = status;
    }

    // Use this from all fragments to set title
    public void setActionBarTitle(String title){
        getSupportActionBar().setTitle(title);
    }

    public void sendNotification(String title, String message) {
        NotificationManagerCompat notificationManager;
        notificationManager = NotificationManagerCompat.from(this);

        Intent intent = new Intent(this, item.getClass());
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        Notification notification = new NotificationCompat.Builder(this, CHANNEL_1_ID)
                .setSmallIcon(R.drawable.ic_action_message_alert)
                .setContentTitle(title)
                .setContentText(message)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .build();

        notificationManager.notify(1, notification);
    }

    private void showNoticeDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View layout = inflater.inflate(R.layout.dialog_privact_notice,null);
        builder.setView(layout);

        final Button buttonOk = layout.findViewById(R.id.button_ok);
        final Button buttonCancel = layout.findViewById(R.id.button_cancel);

        final AlertDialog dialog = builder.show();

        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean("accepted_terms", true);
                editor.apply();
                dialog.dismiss();
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.toolbar_main_activity_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.navigation_admin:
                Intent i;
                if (AdminActivity.getIsAdmin()) {
                    i = new Intent(this, AdminActivity.class);
                }
                else {
                    i = new Intent(this, LoginActivity.class);
                }
                startActivity(i);
                break;
        }
        return true;
    }

    public static void setKill(boolean status) {
        kill = status;
    }

    public boolean getKill() {
        return kill;
    }

    @Override
    public void onBackPressed() {
        SharedPreferences sharedPreferences;
        SharedPreferences.Editor editor;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        if (!kill){
            Intent i = new Intent(this, MainActivity.class);

            this.startActivity(i);
            this.overridePendingTransition(0, 0);
            finish();
        }
        else {
            finish();
        }
    }
}
